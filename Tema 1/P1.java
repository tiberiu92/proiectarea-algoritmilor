import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Tema 1 - Proiectarea Algoritmilor
 * 
 * Problema 1 - Eliminare Gard - Greedy
 */

public class P1 {

	public int N; // nr. de stalpi;
	public int M; // nr. de stalpi ce trebuie eliminati;
	public int L; // lungime gard;
	public int[] v; // vector stalpi;
	public int min; // minimul distantei dintre 2 stalpi;
	public ArrayList<Integer> aux; // vector solutie;

	public void functie() {
		int[] dist = new int[v.length - 1];
		int[] dist_aux = new int[dist.length];

		int lower = 0;
		int upper = L;
		int med, i, sum, M_prim;

		/* vector ce contine distantele dintre stalpi */
		for (int j = 0; j < dist.length; j++) {
			dist[j] = v[j + 1] - v[j];
		}

		while (true) {

			/* conditie pentru iesire din while */
			if (lower > upper) {
				break;

			} else {

				/*
				 * initializare vector solutie si un vector auxiliar pentru
				 * distante
				 */
				aux = new ArrayList<Integer>();
				dist_aux = new int[dist.length];

				M_prim = 0; // nr. de elemente eliminate la fiecare pas;
				sum = 0;
				min = Integer.MAX_VALUE;

				med = lower + (upper - lower) / 2;
				aux.add(v[0]); // adaugam in vectorul solutie primul stalp;

				for (i = 0; i < dist_aux.length; i++) {

					if (sum < med) {
						/*
						 * marcam eliminarea unui stalp prin introducerea nr.
						 * "-1" pe pozitia corespunzatoare ( in vectorul de
						 * distante auxiliar)
						 */
						dist_aux[i] = -1;
						sum += dist[i];

					} else if (sum >= med) {
						aux.add(v[i]);
						dist_aux[i] = sum;
						sum = dist[i];

						if (min > dist_aux[i]) {
							min = dist_aux[i];

						}
					}
				}

				/* introducem ultimul stalp in vectorul solutie */
				dist_aux[i - 1] = sum;
				aux.add(v[v.length - 1]);
				M_prim = v.length - aux.size(); // nr. de elemente eliminate la
												// fiecare pas;

				if (lower == med) {
					break;

				} else if (M_prim > M) {
					/* cazul in care eliminam prea multe elemente */
					upper = med - 1;

				} else if (M_prim < M) {
					/* cazul in care eliminam prea putine elemente */
					lower = med + 1;

				} else if (M == M_prim) {
					/*
					 * cazul in care am eliminat nr. corect de elemente;
					 * comparam distanta dintre ultimii 2 stalpi si minimul
					 * calculat la fiecare pas, in urma eliminarii elementelor
					 */
					if (aux.get(aux.size() - 1) - aux.get(aux.size() - 2) < min) {
						upper = med;

					} else {
						lower = med;

					}

				}

			}

		}

	}

	public static void main(String[] args) {

		P1 p = new P1();

		p.readData("date.in");
		p.functie();
		p.writeData("date.out");

	}

	/* citim datele datele din fisier */
	public void readData(String filename) {
		Scanner scanner = null;
		int i;

		try {

			scanner = new Scanner(new File(filename));
			i = 0;

			N = scanner.nextInt();
			M = scanner.nextInt();
			L = scanner.nextInt();

			v = new int[N];

			while (scanner.hasNextInt()) {
				v[i] = scanner.nextInt();
				i++;
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {

			try {
				if (scanner != null) {
					scanner.close();

				}
			} catch (Exception ex) {
				ex.printStackTrace();

			}
		}

	}

	/* introducem datele obtinute in fisier */
	public void writeData(String filename) {
		BufferedWriter out = null;
		int element;

		try {

			out = new BufferedWriter(new FileWriter(filename));
			out.write(min + "\n" + aux.size() + "\n");

			Iterator<Integer> it = aux.iterator();
			while (it.hasNext()) {
				element = it.next();
				out.write(element + "\n");

			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {

			try {
				if (out != null)
					out.close();

			} catch (Exception ex) {
				ex.printStackTrace();

			}
		}
	}

}
