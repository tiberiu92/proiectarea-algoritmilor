import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class PB {

	public int N;
	public int T;
	public int[] v;
	public int max;

	/*
	 * deplaseaza circular la stanga (circular left shift) elementele din
	 * vector;
	 */
	public void rotate(int[] v, int d, int n) {
		int i;
		for (i = 0; i < d; i++)
			rotateAux(v, n);
	}

	/*
	 * functie auxiliara - deplaseaza la stanga cu un element;
	 */
	public void rotateAux(int[] v, int n) {
		int i;
		int aux;
		aux = v[0];

		for (i = 0; i < n - 1; i++)
			v[i] = v[i + 1];

		v[i] = aux;
	}

	public void functie() {

		int nrows = N - T + 1;
		int ncols = T;
		int[][] matrix = new int[nrows][ncols];
		int i, j;
		int min = Integer.MAX_VALUE;
		int imin = 0;
		max = 0;

		/*
		 * cautam valoarea minima din vector;
		 */
		for (int p = 0; p < v.length; p++) {
			if (min > v[p]) {
				min = v[p];
				imin = p;
			}
		}

		rotate(v, imin, v.length);

		for (i = 0; i < nrows; i++) {
			for (j = 1; j < ncols; j++) {
				if (matrix[i][j - 1] + v[i + j] < matrix[i][j - 1]) {
					max = matrix[i][j - 1];

				} else {
					max = matrix[i][j - 1] + v[i + j];

				}

				if (max > matrix[i][j]) {
					matrix[i][j] = max;

				}

				if (i < nrows - 1 && j < ncols - 1) {
					if (matrix[i][j] < matrix[i][j + 1]) {
						matrix[i + 1][j + 1] = matrix[i][j + 1];

					} else {
						matrix[i + 1][j + 1] = matrix[i][j];

					}
				}
			}
		}

		i = 0;
		j = ncols - 1;

		for (int t = 1; t < nrows; t++) {
			if (matrix[i][j] < matrix[t][j]) {
				i = t;
			}
		}

		max = matrix[i][j];

	}

	public static void main(String[] args) {

		PB p = new PB();
		p.readData("date.in");
		p.functie();
		p.writeData("date.out");

	}

	/* citim datele datele din fisier */
	public void readData(String filename) {
		Scanner scanner = null;
		int i;

		try {

			scanner = new Scanner(new File(filename));
			i = 0;

			N = scanner.nextInt();
			T = scanner.nextInt();

			v = new int[N];

			while (scanner.hasNextInt()) {
				v[i] = scanner.nextInt();
				i++;

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (scanner != null) {
					scanner.close();

				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/* introducem datele obtinute in fisier */
	public void writeData(String filename) {
		BufferedWriter out = null;

		try {

			out = new BufferedWriter(new FileWriter(filename));
			out.write(max + "\n");

		} catch (Exception e) {
			e.printStackTrace();

		} finally {

			try {
				if (out != null)
					out.close();

			} catch (Exception ex) {
				ex.printStackTrace();

			}
		}

	}

}
