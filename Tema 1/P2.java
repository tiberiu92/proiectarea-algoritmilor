import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class P2 {

	public int N;
	public int T;
	public int[] v;
	public int max;

	public void functie() {

		int nrows = N - T + 1;
		int ncols = T;
		int[][] matrix = new int[nrows][ncols];
		int i, j;
		max = 0;

		/*
		 * generam o matrice N x T in care retinem solutiile partiale
		 * (castiguri)
		 */
		for (i = 0; i < nrows; i++) {
			for (j = 1; j < ncols; j++) {
				if (matrix[i][j - 1] + v[i + j] < matrix[i][j - 1]) {
					max = matrix[i][j - 1];

				} else {
					max = matrix[i][j - 1] + v[i + j];

				}

				if (max > matrix[i][j]) {
					matrix[i][j] = max;

				}

				if (i < nrows - 1 && j < ncols - 1) {
					if (matrix[i][j] < matrix[i][j + 1]) {
						matrix[i + 1][j + 1] = matrix[i][j + 1];

					} else {
						matrix[i + 1][j + 1] = matrix[i][j];

					}
				}
			}
		}

		/* cautam maximul de pe ultima coloana */
		i = 0;
		j = ncols - 1;

		for (int t = 1; t < nrows; t++) {
			if (matrix[i][j] < matrix[t][j]) {
				i = t;
			}
		}

		max = matrix[i][j]; // maximul obtinut;

	}

	public static void main(String[] args) {

		P2 p = new P2();
		p.readData("date.in");
		p.functie();
		p.writeData("date.out");

	}

	/* citim datele datele din fisier */
	public void readData(String filename) {
		Scanner scanner = null;
		int i;

		try {

			scanner = new Scanner(new File(filename));
			i = 0;

			N = scanner.nextInt();
			T = scanner.nextInt();

			v = new int[N];

			while (scanner.hasNextInt()) {
				v[i] = scanner.nextInt();
				i++;

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (scanner != null) {
					scanner.close();

				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/* introducem datele obtinute in fisier */
	public void writeData(String filename) {
		BufferedWriter out = null;

		try {

			out = new BufferedWriter(new FileWriter(filename));
			out.write(max + "\n");

		} catch (Exception e) {
			e.printStackTrace();

		} finally {

			try {
				if (out != null)
					out.close();

			} catch (Exception ex) {
				ex.printStackTrace();

			}
		}

	}
}
