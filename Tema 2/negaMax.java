public class negaMax {

	public Field field;
	public static final int directions = 8;
	private static final int MAXDEPTH = 4;
	private static final int MIN_VALUE = Integer.MIN_VALUE;

	public int bestMove(Field field) {

		int max = MIN_VALUE;
		int op = -1;

		Node aux = field.field[field.theball_x][field.theball_y];

		if (field.theball_x == 1) {
			if (field.theball_y == 3) {
				return 1;

			} else if (field.theball_y == 4) {
				return 0;

			} else if (field.theball_y == 5) {
				return 7;
			}

		}

		for (int i = 0; i < directions; i++) {
			if (aux.direction[i] == 0) {
				Field copy = new Field(field);
				copy.move(i);

				int result = negaMaxAlgorithm(copy, MAXDEPTH);
				if (max < result) {
					op = i;
					max = result;

				}
			}
		}

		return op;

	}

	public int negaMaxAlgorithm(Field field, int depth) {
		int max;
		int mingie_x = field.theball_x;
		int mingie_y = field.theball_y;

		Node aux = field.field[mingie_x][mingie_y];

		if (depth == 0) {
			return evaluate(field);

		} else {

			max = MIN_VALUE;

			for (int i = 0; i < directions; i++) {
				if (aux.direction[i] == 0) {
					Field aux_field = new Field(field);

					aux_field.move(i);

					int result = (-1) * negaMaxAlgorithm(aux_field, depth - 1);

					if (max <= result) {
						max = result;

					}
				}
			}

			return max;

		}
	}

	public int evaluate(Field field) {
		int mingie_x = field.theball_x;
		int mingie_y = field.theball_y;

		return (int) Math.signum(mingie_x - 6) * mingie_x
				* (Math.abs(mingie_y - 4) + 1) + (12 - mingie_x) * 40;

	}

}