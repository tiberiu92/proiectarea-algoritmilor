import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Field field = new Field();
		field.FieldGenerator();

		String scan = new String();
		negaMax bot = new negaMax();
		Scanner scanner = new Scanner(System.in);

		while (true) {

			int bestMove;

			scan = scanner.nextLine();

			if (scan.contains("S")) {
				bestMove = bot.bestMove(field);
				field.move(bestMove);

				System.out.println("M 1 " + bestMove);

			}

			if (scan.contains("M")) {
				String[] vector = scan.split(" ");
				int aux = Integer.parseInt(vector[1]);
				ArrayList<Integer> move = new ArrayList<Integer>();

				for (int i = 2; i < aux + 2; i++) {
					field.move(Integer.parseInt(vector[i]));
					move.add(Integer.parseInt(vector[i]));

				}

				int i = 0;
				String auxiliar = "";

				while (true) {
					i++;
					bestMove = bot.bestMove(field);
					field.move(bestMove);
					auxiliar += " " + bestMove;

					Node ball = field.field[field.theball_x][field.theball_y];

					if (!ball.isCorner() && !ball.isPassed()) {
						break;

					}
				}

				System.out.println("M " + i + auxiliar);

			}

			else if (scan.equals("F")) {
				break;

			}

		}

		scanner.close();
	}
}
