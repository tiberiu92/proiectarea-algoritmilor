public class Field {

	public int theball_x;
	public int theball_y;
	private final int nr_rows = 13;
	private final int nr_columns = 9;
	public int directie;

	public Node[][] field = new Node[nr_rows][nr_columns];

	public Field(Field aux_field) {

		theball_x = aux_field.theball_x;
		theball_y = aux_field.theball_y;

		for (int i = 0; i < nr_rows; i++) {
			for (int j = 0; j < nr_columns; j++) {

				field[i][j] = new Node(aux_field.field[i][j].x,
						aux_field.field[i][j].y, aux_field.field[i][j].value,
						aux_field.field[i][j].direction);

			}
		}
	}

	public Field() {
		for (int i = 0; i < nr_rows; i++) {
			for (int j = 0; j < nr_columns; j++) {
				field[i][j] = new Node(i, j, 0, new int[] { 0, 0, 0, 0, 0, 0,
						0, 0 });

			}
		}
	}

	public void FieldGenerator() {

		theball_x = 6;
		theball_y = 4;

		for (int i = 0; i < nr_rows; i++) {
			for (int j = 0; j < nr_columns; j++) {

				if (j == 0 || j == 8) {
					field[i][j].value = 1;

				} else if (i == 0 || i == 12) {
					field[i][j].value = 1;

				} else if (i == 1 && (j <= 3 || j >= 5)) {
					field[i][j].value = 1;

				} else if (i == 11 && (j <= 3 || j >= 5)) {
					field[i][j].value = 1;

				} else {
					field[i][j].value = 0;

				}
			}
		}

		for (int i = 0; i < nr_rows; i++) {
			for (int j = 0; j < nr_columns; j++) {

				if (i >= 2 || i <= 10) {

					if (j == 0) {
						field[i][j].assignDirection(new int[] { 1, 0, 0, 0, 1,
								1, 1, 1 });

					} else if (j == 1) {
						field[i][j].assignDirection(new int[] { 0, 0, 0, 0, 0,
								0, 0, 1 });

					} else if (j == 7) {
						field[i][j].assignDirection(new int[] { 0, 1, 0, 0, 0,
								0, 0, 0 });

					} else if (j == 8) {
						field[i][j].assignDirection(new int[] { 1, 1, 1, 1, 1,
								0, 0, 0 });

					} else {
						field[i][j].assignDirection(new int[] { 0, 0, 0, 0, 0,
								0, 0, 0 });

					}

				}

				if (i == 0 || i == 12) {
					if (j == 3) {
						field[i][j].assignDirection(new int[] { 1, 1, 1, 0, 1,
								1, 1, 1 });

					} else if (j == 4) {
						field[i][j].assignDirection(new int[] { 1, 1, 1, 0, 0,
								0, 1, 1 });

					} else if (j == 5) {
						field[i][j].assignDirection(new int[] { 1, 1, 1, 1, 1,
								0, 1, 1 });

					} else if (j == 0 || j == 1 || j == 2 || j == 6 || j == 7
							|| j == 8)
						field[i][j].assignDirection(new int[] { 1, 1, 1, 1, 1,
								1, 1, 1 });

				}
				if (i == 1) {
					if (j == 4) {
						field[i][j].assignDirection(new int[] { 0, 0, 0, 0, 0,
								0, 0, 0 });
					} else if (j == 1 || j == 2 || j == 6 || j == 7) {
						field[i][j].assignDirection(new int[] { 1, 1, 1, 0, 0,
								0, 1, 1 });
					} else if (j == 0) {
						field[i][j].assignDirection(new int[] { 1, 1, 1, 0, 1,
								1, 1, 1 });
					} else if (j == 3) {
						field[i][j].assignDirection(new int[] { 1, 0, 0, 0, 0,
								0, 1, 1 });
					} else if (j == 5) {
						field[i][j].assignDirection(new int[] { 1, 1, 1, 0, 0,
								0, 0, 0 });
					} else if (j == 8) {
						field[i][j].assignDirection(new int[] { 1, 1, 1, 1, 1,
								0, 1, 1 });
					}

				}
				if (i == 11) {
					if (j == 4) {
						field[i][j].assignDirection(new int[] { 0, 0, 0, 0, 0,
								0, 0, 0 });
					} else if (j == 1 || j == 2 || j == 6 || j == 7) {
						field[i][j].assignDirection(new int[] { 0, 0, 1, 1, 1,
								1, 1, 0 });
					} else if (j == 0) {
						field[i][j].assignDirection(new int[] { 1, 0, 1, 1, 1,
								1, 1, 1 });
					} else if (j == 3) {
						field[i][j].assignDirection(new int[] { 0, 0, 0, 0, 1,
								1, 1, 0 });
					} else if (j == 5) {
						field[i][j].assignDirection(new int[] { 0, 0, 1, 1, 1,
								0, 0, 0 });
					} else if (j == 8) {
						field[i][j].assignDirection(new int[] { 1, 1, 1, 1, 1,
								1, 1, 0 });
					}
				}
			}
		}
	}

	public void move(int direction) {

		switch (direction) {

		case 0:

			if (theball_x > 0) {
				field[theball_x][theball_y].direction[direction] = 1;
				field[theball_x][theball_y].passed += 1;
				theball_x = theball_x - 1;
				field[theball_x][theball_y].direction[direction + 4] = 1;
				directie = direction;

			}

			break;

		case 1:

			if (theball_x > 0 && theball_y < 12) {
				field[theball_x][theball_y].direction[direction] = 1;
				field[theball_x][theball_y].passed += 1;
				theball_x = theball_x - 1;
				theball_y = theball_y + 1;
				field[theball_x][theball_y].direction[direction + 4] = 1;
				directie = direction;

			}

			break;

		case 2:

			if (theball_x < 8) {
				field[theball_x][theball_y].direction[direction] = 1;
				field[theball_x][theball_y].passed += 1;
				theball_y = theball_y + 1;
				field[theball_x][theball_y].direction[direction + 4] = 1;
				directie = direction;

			}

			break;

		case 3:

			if (theball_x < 12 && theball_y < 8) {
				field[theball_x][theball_y].direction[direction] = 1;
				field[theball_x][theball_y].passed += 1;
				theball_x = theball_x + 1;
				theball_y = theball_y + 1;
				field[theball_x][theball_y].direction[direction + 4] = 1;
				directie = direction;

			}

			break;

		case 4:

			if (theball_x < 12) {
				field[theball_x][theball_y].direction[direction] = 1;
				field[theball_x][theball_y].passed += 1;
				theball_x = theball_x + 1;
				field[theball_x][theball_y].direction[(direction + 4) % 8] = 1;
				directie = direction;

			}

			break;

		case 5:

			if (theball_x < 12 && theball_y > 0) {
				field[theball_x][theball_y].direction[direction] = 1;
				field[theball_x][theball_y].passed += 1;
				theball_x = theball_x + 1;
				theball_y = theball_y - 1;
				field[theball_x][theball_y].direction[(direction + 4) % 8] = 1;
				directie = direction;

			}

			break;

		case 6:
			if (theball_y > 0) {
				field[theball_x][theball_y].direction[direction] = 1;
				field[theball_x][theball_y].passed += 1;
				theball_y = theball_y - 1;
				field[theball_x][theball_y].direction[(direction + 4) % 8] = 1;
				directie = direction;

			}

			break;

		case 7:

			if (theball_x > 0 && theball_y > 0) {
				field[theball_x][theball_y].direction[direction] = 1;
				field[theball_x][theball_y].passed += 1;
				theball_x = theball_x - 1;
				theball_y = theball_y - 1;
				field[theball_x][theball_y].direction[(direction + 4) % 8] = 1;
				directie = direction;

			}

			break;
		}

	}

}