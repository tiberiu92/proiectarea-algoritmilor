public class Node {

	public int x;
	public int y;
	public int value;
	public int passed;
	public int[] direction = new int[8];

	public Node(int x, int y, int value, int[] direction) {
		this.x = x;
		this.y = y;
		this.value = value;
		this.passed = 0;

		for (int i = 0; i < direction.length; i++) {
			this.direction[i] = direction[i];

		}

	}

	public void assignDirection(int[] direction) {
		for (int i = 0; i < direction.length; i++) {
			this.direction[i] = direction[i];

		}
	}

	public boolean isCorner() {
		if (x == 0 || x == 12) {
			return false;

		}

		return (value == 1) ? true : false;

	}

	public boolean isPassed() {
		return (passed > 0) ? true : false;

	}

	public int evaluate() {
		return (int) Math.signum(x - 6) * x * (Math.abs(y - 4) + 1) + (12 - x)
				* 45;

	}

}
